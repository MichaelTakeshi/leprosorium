#encoding: utf-8
require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'pry'
require 'sqlite3'

def init_db
	@db = SQLite3::Database.new 'leprosorium.db'
	@db.results_as_hash = true
end


#викликається кожного разу при перезагрузці любої сторінки
before do
	#ініціалізація бази даних
	init_db
end

#викликається кожен раз при конфігурації апки

configure do
	init_db
	#створює таблицю якщо таблиця не створена
	@db.execute 'CREATE TABLE IF NOT EXISTS Posts
	(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		created_date DATE,
		content TEXT,
		user_name TEXT
	)'

	#створює таблицю якщо таблиця не створена
	@db.execute 'CREATE TABLE IF NOT EXISTS Comments
	(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		created_date DATE,
		content TEXT,
		post_id INTEGER
	)'

end

get '/' do
	#вибираємо список постів з Бд
	@results = @db.execute 'select * from Posts order by id desc'

	erb :index
end

get '/new' do
  erb :new
end

#обработчік get-запроса /new
#(браузер получає сторінку з сервера)

post '/new' do
	@content = params[:content] #получаємо перемінну з пост запроса
	@user_name = params[ :user_name]
	if @content.length <= 1
		@error = 'Type post text'
		return erb :new
	end

	#зберігання даних в БД
	@db.execute 'insert into Posts (content, created_date, user_name) values (?,datetime(),?)', [@content, @user_name]

	#перенаправлення на головну сторінку
	redirect to '/'
end

#обработчік post-запроса /new
#(браузер відправляє дані на сервер)

get '/main' do
  "Hello World"
end

#вивід інформації про пост
get '/details/:post_id' do
	post_id = params[:post_id] #получаємо перемінну з урл

	#получаємо список постів
	results = @db.execute 'select * from Posts where id = ?', [post_id]
	@row = results.first.transform_keys(&:to_sym) #індекс 0 тобто сама перша строка

	#вибираємо коментарі до нашого поста
	@comments = @db.execute 'select * from Comments where post_id = ? order by id', [post_id]
	@comments.map! {|comment| comment.transform_keys(&:to_sym)}
#binding.pry

	erb :details #повертаємо представлення детаілс
end


#Обробник пост запроса (бразуер відправляє дані на сервер а ми їх приймаємо)
post '/details/:post_id' do
	post_id = params[:post_id] #получаємо перемінну з урл
	@content = params[:content] #получаємо перемінну з пост запроса

#	if @content.length <= 1
#		@error = 'Type comment text'
#		redirect to('/details/' + post_id)
#	end

	#Зберігання даних в ДБ
	@db.execute 'insert into Comments
	 (
		 content,
		 created_date,
		 post_id
		 )
		 values
		 	(
			 	?,
				 datetime(),
				 ?
			)', [@content, post_id]

	redirect to('/details/' + post_id) #перенаправлення на сторінку поста
end
